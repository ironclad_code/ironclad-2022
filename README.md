# Ironclad 2022

Ironclad Robot project for the First FRC 2022 season

## API Information

[CTRE Labview API](https://github.com/CrossTheRoadElec/Phoenix-Releases/releases/tag/v5.20.2.2)
[REV Labview API](https://docs.revrobotics.com/sparkmax/software-resources/spark-max-api-information#labview)
[NavX Labview API](https://pdocs.kauailabs.com/navx-mxp/software/roborio-libraries/labview/)
[Playing with fusion API](https://www.playingwithfusion.com/docview.php?docid=1205&catid=9012)
[Photon Lib Labview](https://github.com/jsimpso81/PhotonVisionLabVIEW)
[Labview Trajectory Lib](https://github.com/jsimpso81/FRC_LV_TrajLib)
