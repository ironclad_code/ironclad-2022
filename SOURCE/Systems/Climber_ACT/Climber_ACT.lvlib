﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*!!!!*Q(C=\&gt;;^&lt;2N"%-8R:]/"5Q+C9Y-N4!MMA-E&amp;&lt;'";9+K1,&lt;T1!2/WQ"9GN!)(&lt;)%N50^&gt;L7D[!Z)$#2!-\7H*OX?\/T`&gt;(5[3?PMM@&gt;,WO&lt;:[ML8TP&lt;@^R]_?08[PNL_=[=(F?*T`/8^\G&lt;`[W`SL5;P@RG\`L$`WH`9`U`4?XNP`X7\0`^"?F`"";D8/Z[+&lt;HP21+XMO]C)P]C)P]C)XO=F.&lt;H+4GTT*ETT*ETT*ETT)ATT)ATT)A\RPZ#)8O=AB&amp;=7,1M7CR1,&amp;9#AK0AJ0Y3E]B9&gt;$&amp;:\#5XA+4_&amp;BC!J0Y3E]B;@Q-%W&amp;J`!5HM*4?&amp;CK3[JPZ(A+$]P,?)T(?)T(?#AJYT%!5]QM&lt;";")803\"C0]2A0OT)?YT%?YT%?4MNYD-&gt;YD-&gt;YG.+PCLOG&lt;?2Y7%;**`%EHM34?&amp;B;C3@R**\%EXAIJ]34?"*%5D":()+33=G!Z#$R*"[_F(A34_**0)G(5`U/:&lt;]S4&gt;-W=DS"*`!%HM!4?&amp;B#A3@Q"*\!%XB96I%H]!3?Q".Y++8!%XA#4Q!*CF*?Q7,"R'"1%!1?@PL4%PUOO5OC&lt;^*Y?$5?3IW(4?-BUHAY.'[[RMX5O%E;&amp;V`DIGJ=,)W,I0(,;;!V-"J&amp;.#;XA4LR?;1@[(N[_[OWI5`U*8V"H\7J,TTQ&gt;$LJ?$TK=$BIP^_X@T;UW7QU4:/7S[57CY6GM^HF.@#6\@*#?(APX&lt;,PG`70X:?\_&lt;@ZWLO&lt;;?\P[\O7P`Z\[3W]'`62VT7Y2P=SQTBW!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Cluster_Climber Motor Sensor Data.ctl" Type="VI" URL="../Cluster_Climber Motor Sensor Data.ctl"/>
	<Item Name="Cluster_Climber Sensor Data.ctl" Type="VI" URL="../Cluster_Climber Sensor Data.ctl"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="Cluster_Pneumatic Status.ctl" Type="VI" URL="../Cluster_Pneumatic Status.ctl"/>
	<Item Name="Cluster_Timing Data.ctl" Type="VI" URL="../Cluster_Timing Data.ctl"/>
	<Item Name="Conver Arrays to-from String.vi" Type="VI" URL="../Conver Arrays to-from String.vi"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_Climber Mode.ctl" Type="VI" URL="../ENUM_Climber Mode.ctl"/>
	<Item Name="ENUM_Lifter Position Select.ctl" Type="VI" URL="../ENUM_Lifter Position Select.ctl"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Save-Read Position Data to File.vi" Type="VI" URL="../Save-Read Position Data to File.vi"/>
	<Item Name="Sync Position Array to ENUM.vi" Type="VI" URL="../Sync Position Array to ENUM.vi"/>
	<Item Name="Update Climber Position.vi" Type="VI" URL="../Update Climber Position.vi"/>
</Library>
