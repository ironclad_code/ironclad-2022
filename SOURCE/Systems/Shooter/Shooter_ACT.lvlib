﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!*Q(C=\&gt;1^4C."%)&lt;BDR5"Q33_!&lt;+Y16X"6`!6[AJ/#3V3B%2NO"+*QUW&gt;&lt;FD;Q")"A;`A+ZCXW_6B#6A4A,2;U?UWY[``(HJ')`6S):VL=[I-@SWNH^:KP[ZA-RQ_B\ZB/)YY@)W@'F-T8P?0]Y_TXZK`'4O/_Q]PK.0_%U6@Z;P]X_6[`Y\SO91TK?WRXS=N;%YTN7XX3:\E3:\E3:\E12\E12\E12\E4O\E4O\E4O\E2G\E2G\E2G\EP:+,8/1CBZ2MHGS5,*IME!S'IO1L]33?R*.Y_+H%EXA34_**0!R2YEE]C3@R*"[G+@%EHM34?")03X6*^EK/*`'QP!*0Y!E]A3@QM+5#4Q!).AM7$B;"I;!TO!A]A3@Q=+H!%XA#4_!*0(1L]!3?Q".Y!A^4_KF%V\2+DI&gt;FZ(A=D_.R0)[(J?6Y()`D=4S/B_XE?"S0AX!W&gt;";()'?3-]$ZY8A=$X`E?"S0YX%]DI?O@I?]HUT4N%K/R`!9(M.D?!Q03]DQ'"\$9XA-$]P+]"A?QW.Y$!^&lt;S@!9(M.D1)R.W6\'9M:%9Z!2'"Y_`7GR@J?C3[R8K2Z?V5/J?NB5$Z(KY6$&gt;&gt;.8.6.UEV?'L$F6V7+J$50VT+L1+I^J%.&lt;E.V)\P,7V.7^'7N!6N4JP2JL2*G`L"!X?\H&lt;&lt;&lt;L&gt;&lt;LN6;LF:&lt;,J2;,B?&lt;TO7;TG;&lt;4K3;4S@A;O+3/,Y4$?_G;[_`XPZ^_0&amp;T&gt;X4Z=0&gt;XQ^_@^L]?7@`Z\[6^Y._K&lt;`NS$-XI'31Y]YQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Auto Select Speed.vi" Type="VI" URL="../Auto Select Speed.vi"/>
	<Item Name="Calc Speed from Distance.vi" Type="VI" URL="../Calc Speed from Distance.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="Cluster_Shooter Status.ctl" Type="VI" URL="../Cluster_Shooter Status.ctl"/>
	<Item Name="Cluster_Speed vs Distance Table.ctl" Type="VI" URL="../Cluster_Speed vs Distance Table.ctl"/>
	<Item Name="Convert CTRE Encoder Velocity to RPM.vi" Type="VI" URL="../../../Templates and Utilities/RoboRIO Utilities/Convert CTRE Encoder Velocity to RPM.vi"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_Mode Select.ctl" Type="VI" URL="../ENUM_Mode Select.ctl"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Interpolate from  2D Array.vi" Type="VI" URL="../../../Templates and Utilities/RoboRIO Utilities/Interpolate from  2D Array.vi"/>
	<Item Name="Shooter Config Read-Save to File.vi" Type="VI" URL="../Shooter Config Read-Save to File.vi"/>
	<Item Name="Shooter Table Read-Save to File.vi" Type="VI" URL="../Shooter Table Read-Save to File.vi"/>
	<Item Name="TESTER.vi" Type="VI" URL="../TESTER.vi"/>
	<Item Name="Velocity Sensor Data.vi" Type="VI" URL="../Velocity Sensor Data.vi"/>
</Library>
