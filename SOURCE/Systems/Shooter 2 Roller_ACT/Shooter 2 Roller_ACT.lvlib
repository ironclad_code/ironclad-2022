﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;1R&lt;NN!%)8B&amp;].&amp;7NX!5*&gt;[LK"'L1"&gt;94L8;NU19/FW/F="L#LO!KA09'#OI#PI#MK`SZ(ERH;+'!A#,\7U^,A\_XF*5/LNKX3NY&lt;WW?L.R@2L3T_V5H_'3L+9_4*=O1WL=[DT`Z@8,`%HQ_PRB/.7@CASHTSFZW`^/UW@\&lt;0^XOTP_1@N9QB?JL8%]*DXI4D?V:9^*HO2*HO2*HO2"(O2"(O2"(O2/\O2/\O2/\O2'&lt;O2'&lt;O2'&lt;O4^)"?ZS%5/+6E]73AJGB2)"E.2=EI]C3@R*"Z_+P%EHM34?")01Z2Y%E`C34S*BWF+0)EH]33?R%/J,MF_E/.*0*28Y!E]A3@Q""[76/!*!-&amp;C1?'A#!Q&amp;&amp;Y-PA3@Q""[_+P!%HM!4?!)0FR6Y!E`A#4S"BSF^6[*LWE'/BT*S0)\(]4A?RU.J/2\(YXA=D_.B/4E?R_-AH!7&gt;YB$E4()'/$]=D_0BDRS0YX%]DM@R=+H@)?]\UT4N)-&gt;D?!S0Y4%]BI=3-DS'R`!9(M.$72E?QW.Y$)`B93E:(M.D?!S)M3D,SSBG4$1''9(BY&gt;/@&amp;ONX+&lt;L%_C(6Q[N[+&amp;50G_IB5DU=KJOOOJGKG[4;@.7GKD:,N1GK@U[&amp;6G&amp;5C[AGNY%[=.\4&gt;`1N@;2P['P[AD[HT^L5PTTQ=$BIP^^LN^NJO^VK(%&gt;N.BONVWMN&amp;AP.ZX0.:L0T;_#'Y`R#G.Z,&gt;XT`]7PZ.$YOP^U`,Z]?_0PT__VDST`_P@1PP"NVJ:&gt;LM%?`!30\/'E!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Auto Select Speed.vi" Type="VI" URL="../Auto Select Speed.vi"/>
	<Item Name="Calc Speed from Distance.vi" Type="VI" URL="../Calc Speed from Distance.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="Cluster_Shooter Status.ctl" Type="VI" URL="../Cluster_Shooter Status.ctl"/>
	<Item Name="Cluster_Speed vs Distance Table.ctl" Type="VI" URL="../Cluster_Speed vs Distance Table.ctl"/>
	<Item Name="Convert CTRE Encoder Velocity to RPM.vi" Type="VI" URL="../../../Templates and Utilities/RoboRIO Utilities/Convert CTRE Encoder Velocity to RPM.vi"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_Mode Select.ctl" Type="VI" URL="../ENUM_Mode Select.ctl"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Interpolate from  2D Array.vi" Type="VI" URL="../../../Templates and Utilities/RoboRIO Utilities/Interpolate from  2D Array.vi"/>
	<Item Name="Shooter Config Read-Save to File.vi" Type="VI" URL="../Shooter Config Read-Save to File.vi"/>
	<Item Name="Shooter From Distance.vi" Type="VI" URL="../Shooter From Distance.vi"/>
	<Item Name="Shooter Table Read-Save to File.vi" Type="VI" URL="../Shooter Table Read-Save to File.vi"/>
	<Item Name="TESTER.vi" Type="VI" URL="../TESTER.vi"/>
	<Item Name="Velocity Sensor Data.vi" Type="VI" URL="../Velocity Sensor Data.vi"/>
</Library>
