﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;1R&lt;NN!%)8BZS"&amp;7N6K$&amp;VBA*R!6V#J&gt;AI$&lt;K-S,H7&amp;/5#+#("+.\L#./L.+_A):P\&gt;D"A&lt;-+Q!M9%A]&amp;)L5W_8/Z_8"+8?0EE@.4`8$C_W.D\V^GHTJ^`T^P?5V(E&amp;TYV0[;00E`(4_?%8&lt;0Z-`=0P_;@RF`VHGN\&lt;?`O`W]XY"_VN#2&gt;3KT'/31_[UUWN\*DE3:\E3:\E32\E12\E12\E1?\E4O\E4O\E4G\E2G\E2G\E2NY0=J',8/31EO**I742:)&amp;E-B1F8YEH]33?R-.0*:\%EXA34_*BCB*0YEE]C3@R=*E34_**0)EH]&lt;"5FW1`S0%E(J:8Y!E]A3@Q""Z++P!%A+"9M(#Q#!Q&amp;A]&amp;*Y!E]A9&gt;4":\!%XA#4_"B7)%H]!3?Q".YO+4P3H2./]DRM)Q=D_.R0)\(]&lt;#U()`D=4S/R`&amp;14I\(]4A)J[#T/!1Z&amp;TE4H"_/R`(Q2Y\(]4A?R_.Y'/JXS0P/.%U\S0%9(M.D?!S0Y7%*'2\$9XA-D_&amp;B72E?QW.Y$)`BI:1-D_%R0!&lt;%+%JZ'9M:&amp;RK4D-$Q]/F0C`7\&amp;&amp;VC`:$KY65^F+K(4@51K2Y/V5V8X5T646*NPGJ46:OFWA460[&gt;#KT#K)KK,WU1&gt;_2\I?`K/PK6P[#P[EL[AT^KFLTTR?$RK'!&lt;N^XPN&gt;DNNNVNN.BON6CMNFUMN&amp;AP.:L0J.8$*-&lt;U1&gt;../PI\DAV^`O6J`_X'V`HZ\N\[^`_T8QVX,``[^=\&lt;^!_^'@&gt;$D'OT24X`V3QQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_CMD_Select.ctl" Type="VI" URL="../ENUM_CMD_Select.ctl"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
</Library>
