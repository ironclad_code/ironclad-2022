﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*!!!!*Q(C=\&gt;3^&lt;2N"%)&lt;BTY9#!9\9A=%7"F!&amp;B$NA!QYG&gt;-A,T:#"'JA'&amp;$#W)L9Q#2.F&lt;/&amp;+U/H&gt;^9$[A7U+E!U9!P?YR_/X@Q`X$C@V=CF&gt;[./J=P0(UNJ&lt;J`&lt;6,RZ0&gt;&gt;%[V?@:K7=PWJ_-`W8\\]@`B$SWV]_&lt;U`Y42?&gt;S,O_\L+&gt;8F(^,_##V.;9JK5&amp;VKKEN/S6ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE$OZETOZETOZEROZE2OZE2OZE@?$8/1C&amp;TGE:0&amp;EI743:)+E-R1FJ]34?"*0YO'H%E`C34S**`(129EH]33?R*.Y'+&lt;%EXA34_**0%T6*&gt;E0=DS*B_E6?!*0Y!E]A9=F&amp;8A#1,"9-(%Q#1Q&amp;D=&amp;&amp;Y!E]A9&gt;,":\!%XA#4_#B79%H]!3?Q".Y'.*X*&lt;KG(?2YG%;/R`%Y(M@D?*B;DM@R/"\(YXB94I\(]4A):U&amp;H=ABS"DE&gt;H"_/R`(Q*=@D?"S0YX%].05\Z(VHGK9&gt;Z(A-D_%R0)&lt;(]$#&amp;$)`B-4S'R`!QL1S0Y4%]BM@QM*1-D_%R0!&lt;%7*4F:5RG$$1['9(BY&gt;/@&amp;ONX+&lt;L%_C(6Q[N[+&amp;50G_IB5DU=KJOOOJGKG[4;@.7GKD:,N1GK0[&gt;#KT#K263$7U?.H!`5(86,X6!([J+[I-[JMT&lt;U,X==RV'(QU'\X5\&lt;\6;&lt;T5&lt;$-'CZ8'KR7'A_HWMWGRV@!Z]ZDC]%L&gt;P&amp;^WG[`\K``L+[ORV8__OR@1^80\[V`/XPH:0F0XAX[K/?LM%?01#GV3F4!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ACT_Loop.vi" Type="VI" URL="../ACT_Loop.vi"/>
	<Item Name="Cluster_CMD_ACT_Data.ctl" Type="VI" URL="../Cluster_CMD_ACT_Data.ctl"/>
	<Item Name="Cluster_Turret Status.ctl" Type="VI" URL="../Cluster_Turret Status.ctl"/>
	<Item Name="Cluster_Vision Data.ctl" Type="VI" URL="../Cluster_Vision Data.ctl"/>
	<Item Name="Conver Arrays to-from String.vi" Type="VI" URL="../Conver Arrays to-from String.vi"/>
	<Item Name="ENUM CMD Mode.ctl" Type="VI" URL="../ENUM CMD Mode.ctl"/>
	<Item Name="ENUM_ACT_Action.ctl" Type="VI" URL="../ENUM_ACT_Action.ctl"/>
	<Item Name="ENUM_ACT_State.ctl" Type="VI" URL="../ENUM_ACT_State.ctl"/>
	<Item Name="ENUM_Turret Position Select.ctl" Type="VI" URL="../ENUM_Turret Position Select.ctl"/>
	<Item Name="Global_ACT_Data Out.vi" Type="VI" URL="../Global_ACT_Data Out.vi"/>
	<Item Name="Global_CMD_ACT_Data In.vi" Type="VI" URL="../Global_CMD_ACT_Data In.vi"/>
	<Item Name="Save-Read Position Data to File.vi" Type="VI" URL="../Save-Read Position Data to File.vi"/>
	<Item Name="Sync Position Array to ENUM.vi" Type="VI" URL="../Sync Position Array to ENUM.vi"/>
</Library>
